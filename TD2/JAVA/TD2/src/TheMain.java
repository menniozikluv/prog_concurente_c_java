public class TheMain {
    public static void main(String[] args) {

        TacheTheMain t1 = new TacheTheMain("Souvent, pour s'amuser, les hommes d equipage,\n");
        TacheTheMain t2 = new TacheTheMain("Prennent des goelands, vastes oiseaux des mers,\n");
        TacheTheMain t3 = new TacheTheMain("Qui suivent, indolents compagnon de voyage,\n");
        TacheTheMain t4 = new TacheTheMain("Le navire glissant sur les gouffres amers.\n");

        try {
            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();
            t3.join();
            t4.start();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }
}
