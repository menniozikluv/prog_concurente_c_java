public class Tache implements Runnable{

    String msg;

    public Tache (String msg){
        this.msg = msg;
    }

    @Override
    public void run() {
        System.out.println(msg);
    }
}
