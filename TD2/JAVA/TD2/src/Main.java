public class Main {
    public static void main(String[] args) {
        Thread th1, th2, th3, th4;
        Tache t1 = new Tache("Souvent, pour s'amuser, les hommes d equipage,\n");
        Tache t2 = new Tache("Prennent des goelands, vastes oiseaux des mers,\n");
        Tache t3 = new Tache("Qui suivent, indolents compagnon de voyage,\n");
        Tache t4 = new Tache("Le navire glissant sur les gouffres amers.\n");

        th1 = new Thread(t1);
        th2 = new Thread(t2);
        th3 = new Thread(t3);
        th4 = new Thread(t4);

        try {
            th1.start();
            th1.join();
            th2.start();
            th2.join();
            th3.start();
            th3.join();
            th4.start();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}