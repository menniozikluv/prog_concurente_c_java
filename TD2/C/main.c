#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *printArg (void *arg) {
    char* c = (char *) arg;
        printf("%s\n", c);
        pthread_exit(NULL);
}

int main() {

    //Question 1 : Ecrire ce programme (mono tâche) qui satisfait ces spécifications
    /*printf("Souvent, pour s'amuser, les hommes d'equipage,\n");
    printf("Prennent des goelands, vastes oiseaux des mers,\n");
    printf("Qui suivent, indolents compagnon de voyage,\n");
    printf("Le navire glissant sur les gouffres amers.\n");*/

    //Question 2 :
    pthread_t th1, th2, th3, th4;

    char *chaine1 = "Souvent pour s amuser, les hommes d equipage,";
    char *chaine2= "Prennent des goelands vastes oiseaux des mers,";
    char *chaine3= "Qui suivent, indolents compagnon de voyage,";
    char *chaine4= "Le navire glissant sur les gouffres amers.";

    pthread_create(&th1, NULL, printArg, (void*) chaine1);
    pthread_create(&th2, NULL, printArg, (void*) chaine2);
    pthread_create(&th3, NULL, printArg, (void*) chaine3);
    pthread_create(&th4, NULL, printArg, (void*) chaine4);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);
    pthread_join(th3, NULL);
    pthread_join(th4, NULL);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <pthread Posix.h>

void *affCar(void *arg) {
    char c;
    c = * (char *) arg;
    for(int i=0; i<10; i++){
        putchar(c);
    }

    int main (void) {
        char leCar ='A';
        pthread_t tache1;
        pthread_t tache2;

        leCar = 'B';
        pthread_create(&tache1, NULL, affCar, (void*) &leChar);

        leCar = 'C';
        pthread_create(&tache2, NULL, affCar, (void*) &leChar);

        for(int i=0; i<10; i++){
            putchar('z');
        }

    }
}

public class Filtre1 extends Thread {
    private Buffer1 buffGauche;
    private Buffer1 buffDroit;
    private String pattern;
    private char remplacement;

    public void run() {
        char c;
        do {
            c = (char) buffGauche.Get();
            if(buffDroit != null){
                c = remplacement,
                buffDroit.Put(c);
            }
            else {
                System.out.print(c);
            }
        } while (c != 'O');
    }
}

