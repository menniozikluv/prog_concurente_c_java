public class GSGN extends Thread {
    private Controleur theControleur;
    private int tempsParcours; // en ms
    private int tempsAvantDemmarage; // en ms
    private int trainNumber;

    public GSGN(Controleur theControleur,int tempsAvantDemmarage, int tempsParcours, int trainNumber) {
        super();
        this.theControleur = theControleur;
        this.tempsParcours = tempsParcours;
        this.tempsAvantDemmarage = tempsAvantDemmarage;
        this.trainNumber = trainNumber;
    }

    public void run() {
        try {
            Thread.sleep(this.tempsAvantDemmarage);
            theControleur.entreeGs();
            System.out.println("Le train "+ this.trainNumber + " vient de renter Gare Sud");
            Thread.sleep(this.tempsParcours);
            theControleur.sortieGn();
            //System.out.println("Le train "+ this.trainNumber + " vient de sortir Gare Nord");
        } catch (Exception e1) {
            System.out.println("Erreur dans la tache GsGn" + this.trainNumber);
            e1.printStackTrace();
        }
    }
}
