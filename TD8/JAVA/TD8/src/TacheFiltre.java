public class TacheFiltre extends Thread {
    private Integer monNombre;
    private Buffer buffGauche;
    private Buffer buffDroit;

    public TacheFiltre(Integer n, Buffer buffEntree) {
        this.monNombre = Integer.valueOf(n);
        this.buffGauche = buffEntree;
        this.buffDroit = null;
    }

    @Override
    public void run() {
        Integer x;
        do {
            x = (Integer) buffGauche.Get();
            if ((x.intValue() % monNombre.intValue()) != 0) {
                // x n'est pas multiple de monNombre --> on passe au voisin droit
                if (buffDroit == null) {
                    // le voisin droit n'existe pas --> on le créé et on le démarre
                    buffDroit = new Buffer();
                    new TacheFiltre(x, buffDroit).start();
                } else {
                    buffDroit.Put(x);
                }
            }
        } while (x.intValue() > 0);
        // on a reçu 0 --> on termine
        System.out.print(monNombre + "  ");
        try {
            sleep(1);
        } catch (InterruptedException e) {
        }
        if (buffDroit != null)
            buffDroit.Put(x);
        else {
            System.out.println();
        }
    }
}
