public class ControleurOk implements Controleur {
        private int countGsGn = 0; // compteur pour les trains de type GsGn
        private int countGnGs = 0; // compteur pour les trains de type GnGs
        private final int N = 2; // seuil pour bloquer les trains en attente

        // méthode pour l'entrée des trains en Gare Sud
        @Override
        public synchronized void entreeGs() throws InterruptedException {
            // bloquer les trains de type GnGs si le compteur pour GsGn est supérieur ou égal à N
            while (countGsGn >= N) {
                wait();
            }
            countGsGn++;
        }

        // méthode pour la sortie des trains en Gare Nord
        @Override
        public synchronized void sortieGn() {
            countGsGn--;
            notifyAll(); // débloquer les trains en attente
        }

        // méthode pour l'entrée des trains en Gare Nord
        @Override
        public synchronized void entreeGn() throws InterruptedException {
            // bloquer les trains de type GsGn si le compteur pour GnGs est supérieur ou égal à N
            while (countGnGs >= N) {
                wait();
            }
            countGnGs++;
        }

        // méthode pour la sortie des trains en Gare Sud
        @Override
        public synchronized void sortieGs() {
            countGnGs--;
            notifyAll(); // débloquer les trains en attente
        }
}
