public interface Controleur {
    public void entreeGn() throws InterruptedException;
    public void entreeGs() throws InterruptedException;
    public void sortieGn() throws InterruptedException;
    public void sortieGs() throws InterruptedException;

}
