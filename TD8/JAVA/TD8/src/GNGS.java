public class GNGS extends Thread {
    private Controleur theControleur;
    private int tempsParcours; // en ms
    private int tempsAvantDemmarage; // en ms
    private int trainNumber;

    public GNGS(Controleur theControleur, int tempsAvantDemmarage, int tempsParcours, int trainNumber) {
        super();
        this.theControleur = theControleur;
        this.tempsParcours = tempsParcours;
        this.tempsAvantDemmarage = tempsAvantDemmarage;
        this.trainNumber = trainNumber;
    }

    public void run() {
        try {
            Thread.sleep(this.tempsAvantDemmarage);
            theControleur.entreeGn();
            System.out.println("        Le train " + this.trainNumber + " vient de renter Gare Nord");
            Thread.sleep(this.tempsParcours);
            theControleur.sortieGs();
            //System.out.println("         Le train "+ this.trainNumber + " vient de sortir Gare Sud");
        } catch (Exception e1) {
            System.out.println("Erreur dans la tache GsGn" + this.trainNumber);
            e1.printStackTrace();
        }
    }
}
