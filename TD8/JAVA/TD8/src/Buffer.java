public class Buffer {
    private Object val;
    private boolean present;

    public Buffer() {
        present = false;
    }

    public synchronized void Put(Object m) {
        while (present)
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        val = m;
        present = true;
        notify();
    }

    public synchronized Object Get() {
        while (!present)
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        Object m = val;
        notify();
        present = false;
        return m;
    }
}
