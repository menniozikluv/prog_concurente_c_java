public class RessourceSynchro implements Ressource{

    int val = 0;

    public int getVal() {
        return val;
    }
    @Override
    public synchronized void dec() throws InterruptedException {
        val--;
    }

    @Override
    public synchronized void inc() throws InterruptedException {
        val++;
    }

    public static void main(String[] args) {
        RessourceSynchro r1 = new RessourceSynchro();
        Incremente inc = new Incremente(r1);
        Decremente dec = new Decremente(r1);

        Thread th1_inc = new Thread(inc);
        Thread th2_dec = new Thread(dec);

        th1_inc.start();
        th2_dec.start();

        try{
            th1_inc.join(); // 1
            th2_dec.join(); // 0

        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("La valeur est =" + r1.getVal());
    }
}
