public class Incremente implements Runnable{

    Ressource r1;

    public Incremente (Ressource r1) {
        this.r1 = r1;
    }

    @Override
    public void run() {
        try {
            r1.inc();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }
}
