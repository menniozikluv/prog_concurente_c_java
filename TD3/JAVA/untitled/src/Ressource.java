public interface Ressource {
    void dec() throws InterruptedException;
    void inc() throws InterruptedException;
}
