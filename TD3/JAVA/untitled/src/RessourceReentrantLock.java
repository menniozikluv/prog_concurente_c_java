import java.util.concurrent.locks.ReentrantLock;

public class RessourceReentrantLock implements Ressource {
    ReentrantLock mutex = new ReentrantLock();
    int val = 0;

    public int getVal() {
        return val;
    }
    @Override
    public void dec() {
        mutex.lock();
        val--;
        mutex.unlock();
    }

    @Override
    public void inc() {
        mutex.lock();
        val++;
        mutex.unlock();
    }

    public static void main(String[] args) {
        RessourceReentrantLock r1 = new RessourceReentrantLock();
        Incremente inc = new Incremente(r1);
        Decremente dec = new Decremente(r1);

        Thread th1_inc = new Thread(inc);
        Thread th2_dec = new Thread(dec);

        th1_inc.start();
        th2_dec.start();

        try{
            th1_inc.join(); // 1
            th2_dec.join(); // 0

        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("La valeur est =" + r1.getVal());
    }
}
