public class Decremente implements Runnable{
    Ressource r1;

    public Decremente (Ressource r1) {
        this.r1 = r1;
    }

    @Override
    public void run() {
        try {
            r1.dec();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
