import java.util.concurrent.Semaphore;

public class RessourceSemaphore implements Ressource {
    Semaphore mutex = new Semaphore(1);
    int val = 0;

    public int getVal() {
        return val;
    }
    @Override
    public void dec() throws InterruptedException {
        mutex.acquire();
        val--;
        mutex.release();
    }

    @Override
    public void inc() throws InterruptedException {
        mutex.acquire();
        val++;
        mutex.release();
    }

    public static void main(String[] args) {
        RessourceSemaphore r1 = new RessourceSemaphore();
        Incremente inc = new Incremente(r1);
        Decremente dec = new Decremente(r1);

        Thread th1_inc = new Thread(inc);
        Thread th2_dec = new Thread(dec);

        th1_inc.start();
        th2_dec.start();

        try{
            th1_inc.join(); // 1
            th2_dec.join(); // 0

        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("La valeur est =" + r1.getVal());
    }
}