public class RessourceBlocSynchro implements Ressource {

    int val = 0;

    public int getVal() {
        return val;
    }

    @Override
    public void dec() throws InterruptedException {
        synchronized (this) {
            val--;
        }
    }

    @Override
    public void inc() throws InterruptedException {
        synchronized (this) {
            val++;
        }
    }

    public static void main(String[] args) {
        RessourceSynchro r1 = new RessourceSynchro();
        Incremente inc = new Incremente(r1);
        Decremente dec = new Decremente(r1);

        Thread[] th1_inc = new Thread[100000];
        Thread[] th2_dec = new Thread[100000];

        for (int i=0; i < 100000; i++) {
            th1_inc[i] = new Thread(inc);
            th2_dec[i] = new Thread(dec);
        }

        for (int i=0; i < 100000; i++) {
            th1_inc[i].start();
            th2_dec[i].start();
        }

        for (int i=0; i < 100000; i++) {
            try {
                th1_inc[i].join();
                th2_dec[i].join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println("La valeur est =" + r1.getVal());
    }
}