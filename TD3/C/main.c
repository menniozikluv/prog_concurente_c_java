#include <stdio.h>
#include <pthread.h>

int Val = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* Dec(){
    pthread_mutex_lock(&mutex);
    Val--;
    pthread_mutex_unlock(&mutex);
}

void* Inc(){
    pthread_mutex_lock(&mutex);
    Val++;
    pthread_mutex_unlock(&mutex);
}

void* tacheInc() {
    for (int i=0; i<1000; i++) {
        Inc();
    }

}

void* tacheDec() {
    for (int i=0; i<1000; i++) {
        Dec();
    }

}

int main() {
    pthread_t th_dec, th_inc;

    pthread_create(&th_dec,NULL,(void *) tacheDec,NULL);
    pthread_create(&th_inc,NULL,(void *) tacheInc,NULL);

    pthread_join(th_dec,NULL);
    pthread_join(th_inc,NULL);

    printf("Valeur finale de val = %d\n", Val);
    return 0;
}
