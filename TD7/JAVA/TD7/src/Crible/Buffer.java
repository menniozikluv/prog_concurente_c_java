package Crible;

public class Buffer {
    Boolean estPresent; //valeur boolean pour savoir si plein ou vide
    Object val;
    public  Buffer() {
        this.estPresent = false;
    }

    public synchronized Object Get() { //consommateur
        while(!this.estPresent) { //vide
            try {
                wait(); //met en attente
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        Object m = val; //garantit que la valeur stockée dans le buffer est récupérée de manière sûre et fiable.
        notify(); //notify les autres threads
        this.estPresent =false;
        return m;
    }

    public synchronized void Put(Object m) { //producteur
        while(this.estPresent){ //plein
            try {
                wait(); // attend qui soit vide
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        this.val = m; //vide on put
        this.estPresent = true; // true pour dire qui est vide
        notify();
    }
}
