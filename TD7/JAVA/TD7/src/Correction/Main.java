package Correction;

public class Main {
    static int N = 10000;
    public static void main(String[] args) {
        Buffer buffDepart = new Buffer();

        new TacheFiltre(Integer.valueOf(2), buffDepart).start();

        for (int i = 3; i < N; i+=2) {
            buffDepart.Put(Integer.valueOf(i));
        }
        buffDepart.Put(Integer.valueOf(0));
    }
}